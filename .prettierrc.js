module.exports = {
  singleQuote: true,
  useTabs: false,
  printWidth: 80,
  tabWidth: 2,
  semi: true,
  arrowParens: 'avoid',
  bracketSpacing: false,
  proseWrap: 'preserve',
  endOfLine: 'lf',
  quoteProps: 'consistent',
};
